let respecConfig = {
    useLogo: true,
    useLabel: true,
    license: "cc-by",
    specStatus: "VV",
    specType: "ST",
    pubDomain: "vv",
    shortName: "logging",
    publishDate: "2023-12-08",
    publishVersion: "0.0.1",

    previousMaturity: "VV",
    title: "FSC - Logging",
    content: {"draft-fsc-logging-00-introduction": "", "draft-fsc-logging-00-architecture": "", "draft-fsc-logging-00-specifications": ""},
    editors:
        [
            {
                name: "VNG Realisatie",
                company: "VNG",
                companyURL: "https://vng.nl/rubrieken/onderwerpen/standaarden",
            }
        ],
    authors:
        [
            {
                name: "Eelco Hotting",
                company: "Hotting IT",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:rfc@hotting.it",
                    }
                ]
            },
            {
                name: "Ronald Koster",
                company: "PhillyShell",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:rfc@phillyshell.nl",
                    }
                ]
            },
            {
                name: "Henk van Maanen",
                company: "AceWorks",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:henk.van.maanen@aceworks.nl",
                    }
                ]
            },
            {
                name: "Niels Dequeker",
                company: "ND Software",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:niels@nd-software.be",
                    }
                ]
            },
            {
                name: "Edward van Gelderen",
                company: "vanG IT",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:e.van.gelderen@vang.nl",
                    }
                ]
            },
            {
                name: "Pim Gaemers",
                company: "Apily",
                extras: [
                    {
                        name: "Email",
                        href: "mailto:pim.gaemers@apily.dev",
                    }
                ]
            },
        ],
};
