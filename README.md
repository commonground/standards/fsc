Draft standard Federated Service Connectivity
--

## Introduction

This is the multipart standard for Federated Service Connectivity (FSC), which specifies a way to implement technically interoperable gateway functionality, covering federated authentication, secure connecting and protocolling in a large-scale, dynamic API landscape. The standard includes the exchange of information and requests about the management of connections and authorizations, in order to make it possible to automate those activities. The standard builds on multiple open standards to create a consistent system.

There are no approved versions of the specification. This is an early draft version.

The FSC standard and its extensions define modular API building blocks to enable the use of API's in a large-scale, dynamic system of cooperating organisations.

## Development

Docker is required in order to generate the RFCs. 
You can edit the Markdown files and regenerate the HTML, TXT and PDFs using `make all`.

## Reference links

- [mmark - markdown syntax reference](https://mmark.miek.nl/post/syntax/)
