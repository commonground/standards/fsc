SRC_PUML  := $(wildcard ./**/**/*.puml)
SVG  := $(patsubst %.puml,%.svg,$(SRC_PUML))
UID  := `id -u`
GID  := `id -g`
CWD  := `pwd`

all: $(SVG) core/draft-fsc-core-00.html logging/draft-fsc-logging-00.html

clean:
	rm -f ./core/draft-fsc-core-00.html ./delegation/draft-fsc-delegation-00.html ./logging/draft-fsc-logging-00.html ./**/**/*.svg

%.svg: %.puml
	cat $< | docker run --rm -i registry.gitlab.com/commonground/nlx/images/plantuml:latest -svg > $@

core/draft-fsc-core-00.html: core/base.html
	docker run --rm -i -v $(CWD):/app/ registry.gitlab.com/commonground/nlx/images/respec:latest --src $< --disable-sandbox true --localhost --out $@

logging/draft-fsc-logging-00.html: logging/base.html
	docker run --rm -i -v $(CWD):/app/ registry.gitlab.com/commonground/nlx/images/respec:latest --src $< --disable-sandbox true --localhost --out $@
